import tweepy
import os

consumer_key = os.environ['consumer_key']
consumer_secret = os.environ['consumer_secret']
access_token = os.environ['access_token']
access_token_secret = os.environ['access_token_secret']

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)
api.create_friendship('fabiomathias')

user = 'veramagalhaes'

# get user last 5 tweets
for tweet in api.user_timeline(id = user, count = 5):
    #check if i've answered
    answer = True
    for mytweet in api.user_timeline():
        if mytweet.in_reply_to_status_id_str != None and tweet.id != None:
            if int(mytweet.in_reply_to_status_id_str) == int(tweet.id):
                answer = False
                break

    #if the post isnt a retweet and i've not answered yet, do the post
    if hasattr(tweet,'retweeted_status') == False and answer == True:
        print("replying to "+ str(tweet.id))
        api.update_status("eh que era uma escolha muito dificil",
         in_reply_to_status_id = tweet.id,
         auto_populate_reply_metadata = 1,
         exclude_reply_user_ids = 0)
