FROM alpine:latest

RUN apk update && \
    apk add dcron curl wget rsync ca-certificates py3-pip && \
    rm -rf /var/cache/apk/*

RUN pip3 install tweepy

RUN mkdir -p /var/log/cron && \
    mkdir -m 0644 -p /var/spool/cron/crontabs && \
    touch /var/log/cron/cron.log && \
    mkdir -m 0644 -p /etc/cron.d

COPY /scripts/* /

RUN crontab -f /hardchoice-crontab && \
  chmod +x hardchoice-entrypoint.sh

CMD [ "/hardchoice-entrypoint.sh" ]
