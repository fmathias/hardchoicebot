### Build

```bash
docker build . -t hardchoice:latest
```

### Configuration

1. Copy env-default .env
2. Config .env

### Run

```bash
docker run --env-file .env -d hardchoice:latest
```
